<div align='center'>
    <h1><b>☕ COFFESES ☕</b></h1>
![Node.js](https://badgen.net/badge/Node.js/v18.16/green?)
</div>

---

## 💾 **ABOUT**

Это frontend часть для coffeses
Все важные технологии коотрые используются на данный момент:

| Package            | Details                                                                                |
| ------------------ | -------------------------------------------------------------------------------------- |
| vite               | ![vite version](https://img.shields.io/npm/v/vite.svg?label=%20)                       |
| vue                | ![vue version](https://img.shields.io/npm/v/vue.svg?label=%20)                         |
| vite-plugin-pwa    | ![vite-plugin-pwa version](https://img.shields.io/npm/v/vite-plugin-pwa.svg?label=%20) |
| daisyui            | ![daisyui version](https://img.shields.io/npm/v/daisyui.svg?label=%20)                 |
| tailwindcss        | ![tailwindcss](https://img.shields.io/npm/v/tailwindcss.svg?label=%20)                 |
| headlessui         | ![headlessui](https://img.shields.io/npm/v/@headlessui/vue.svg?label=%20)              |
| prettier           | ![prettier](https://img.shields.io/npm/v/prettier.svg?label=%20)                       |
| eslint             | ![eslint](https://img.shields.io/npm/v/eslint.svg?label=%20)                           |
| axios              | ![axios](https://img.shields.io/npm/v/axios.svg?label=%20)                             |
| vee-validate       | ![vee-validate](https://img.shields.io/npm/v/vee-validate.svg?label=%20)               |
| vue-router         | ![vue-router](https://img.shields.io/npm/v/vue-router.svg?label=%20)                   |
| yup                | ![yup](https://img.shields.io/npm/v/yup.svg?label=%20)                                 |
| vite-plugin-mkcert | ![vite-plugin-mkcert](https://img.shields.io/npm/v/vite-plugin-mkcert.svg?label=%20)   |

<br />

---

## 🗒️ **Getting Started**

1. clone the repo

```
git clone https://github.com/freeflight-pro/frontend.git **your_directory_name**
```

2. cd into cloned repo

```
cd **your_directory_name**
```

3. install dependencies

```
npm install

```

if you get errors prevent the program from working, try using

```
sudo npm install (this is bad advice i know)

```

4. run the app

```
npm run dev

```

if something breaks the program, try using the previous step again

<br />
