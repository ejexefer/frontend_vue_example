import { fileURLToPath, URL } from 'url'
import { defineConfig } from 'vite'
import Vue from '@vitejs/plugin-vue'
import mkcert from 'vite-plugin-mkcert'
import dns from 'dns'
import eslintPlugin from 'vite-plugin-eslint'

dns.setDefaultResultOrder('verbatim')

export default defineConfig({
  server: {
    https: true,
    host: true,
    changeOrigin: true,
    port: 8080,
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
  plugins: [
    eslintPlugin({
      failOnError: false,
      failOnWarning: false,
    }),
    mkcert(),
    Vue(),
  ],
  build: {
    rollupOptions: {
      input: 'src/main.js',
      output: {
        intro: 'console.log("Запуск Vite!");',
      },
    },
  },
})
