# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2023-05-20

### Added

- Сделан компонент авторизации (и регистрации), протестирован с помощью mock сервера, затем с помощью запущенного backend'а
- Добавлен vite-plugin-mkcert для работы external ссылок, для тестирования на смартфонах и других устройствах в сети
- Добавлен но не настроен stylelint, возможно он не потребуется пока не будет написана своя ui библиотека
- Реализована валидация полей с помощью vee-validate и yup до отправки на сервер
- Ответы ошибок сервера отображаются на странице регистрации или входа
- Добавлена реализация layouts которая в дальнейшем возможно изменится
- Для сборки проекта использован vite вместе со встроенным dev server
- Использован daisyui для прототипирования в связке с tailwindcss
- Настроен проект согласно стеку AirBnb (eslint + prettier)
- Реализован простой роутинг с помощью vue-router
- Для запросов добавлен axios
