import { TestAPIInstance } from '@/api'

// function loadDashboardRecipes() {
//   return TestAPIInstance.get('/dashboard')
// }
// function loadRecipe(recipeUuid) {
//   return TestAPIInstance.get('/dashboard/' + recipeUuid)
// }
//запросы переписаны для того чтобы были mock данные из firebase
function loadDashboardRecipes() {
  return TestAPIInstance.get('/dashboard.json')
}
function loadRecipe(recipeUuid) {
  return TestAPIInstance.get(recipeUuid + '.json')
}

function saveRecipe(recipeUuid, payload) {
  return TestAPIInstance.put('/dashboard/' + recipeUuid, payload)
}

function createRecipe(recipeUuid, payload) {
  return TestAPIInstance.post('/dashboard/' + recipeUuid, payload)
}

function updateRecipe(recipeUuid, payload) {
  return TestAPIInstance.patch('/dashboard/' + recipeUuid, payload)
}
function deleteRecipe(recipeUuid) {
  return TestAPIInstance.delete('/dashboard/' + recipeUuid)
}

export {
  loadDashboardRecipes,
  saveRecipe,
  loadRecipe,
  createRecipe,
  updateRecipe,
  deleteRecipe,
}

export default {
  loadDashboardRecipes,
  saveRecipe,
  loadRecipe,
  createRecipe,
  updateRecipe,
  deleteRecipe,
}
