import { TestAPIInstance } from '@/api'

function loadListMain() {
  return TestAPIInstance.get('/list.json')
}

export { loadListMain }

export default {
  loadListMain,
}
