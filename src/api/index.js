import axios from 'axios'

const defaultConfig = {
  baseURL: import.meta.env.VITE_VUE_APP_BASE_URL,
  withCredentials: true,
  headers: {
    accept: 'application/json',
  },
}

const testConfig = {
  baseURL: import.meta.env.VITE_VUE_APP_TEST_URL,
  // withCredentials: true,
  headers: {
    accept: 'application/json',
  },
}

const DefaultAPIInstance = axios.create(defaultConfig)
const TestAPIInstance = axios.create(testConfig)

export { DefaultAPIInstance, TestAPIInstance }

export default {
  DefaultAPIInstance,
  TestAPIInstance,
}
