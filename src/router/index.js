import { createRouter, createWebHistory } from 'vue-router'
import CardModalView from '@/components/CardModalView.vue'
import SettingsModalView from '@/components/SettingsModalView.vue'
const routes = [
  {
    path: '/',
    name: 'Dashboard',
    meta: { layout: 'main' },
    component: () => import('@/views/DashboardView.vue'),
    props: true,
  },
  {
    path: '/post',
    name: 'DashboardModal',
    meta: { layout: 'main' },
    component: () => import('@/views/DashboardView.vue'),
    props: true,
    children: [
      {
        name: 'DashboardModalView',
        path: ':hash',
        component: CardModalView,
        props: true,
      },
    ],
  },
  {
    path: '/user',
    name: 'DashboardUser',
    meta: { layout: 'main' },
    component: () => import('@/views/DashboardView.vue'),
    props: true,
    children: [
      {
        name: 'SettingsModalView',
        path: ':route',
        component: SettingsModalView,
        props: true,
      },
    ],
  },
  {
    path: '/label/:label',
    name: 'DashboardList',
    meta: { layout: 'main' },
    component: () => import('@/views/DashboardView.vue'),
    props: true,
    children: [
      {
        name: 'DashboardModalView2',
        path: ':hash',
        component: CardModalView,
        props: true,
      },
    ],
  },
  {
    path: '/signin',
    name: 'SignIn',
    meta: { layout: 'auth' },
    component: () => import('@/views/SignInView.vue'),
  },
  {
    path: '/signup',
    name: 'Register',
    meta: { layout: 'auth' },
    component: () => import('@/views/RegisterView.vue'),
  },
]

const router = createRouter({
  routes,
  history: createWebHistory(import.meta.env.BASE_URL),
})

export default router
